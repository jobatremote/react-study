import React from 'react'

const Hello = () => {
  return React.createElement(
              'div',
              {id: 'hello_id', className: 'dummyClass'},
              React.createElement('h3', null,
                  'The component with simple js'))
}

export default Hello;
