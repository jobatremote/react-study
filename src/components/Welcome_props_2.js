import React from 'react'

// Destruction props
const Props2 = ({name, swordColor, children}) => {
  return (
    <div>
      <h2>{name} has his sword color {swordColor}</h2>
      {children}
    </div>
  )
}

export default Props2;
