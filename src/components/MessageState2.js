import React, { Component } from 'react';

class Welcome extends Component {
  constructor() {
    super()
    this.state = { message: 'Welcome visitor!', alt: 'Efa @10h00!!!'  }
    this.changeMessage = this.changeMessage.bind(this)
  }


  changeMessage() {
    this.setState({
      message: this.state.alt,
      alt: this.state.message 
    })
  }
  
  render() {
    return (
      <div>
        <span>
          {/*  <button onClick={ this.changeMessage.bind(this) }>Change msg</button> */}
          {/*  <button onClick={ () => this.changeMessage() }>Change msg</button> */}
          <button onClick={ this.changeMessage }>Change msg</button>
        </span>
        <span style={{ marginLeft: '10px' }}>
          { this.state.message }
        </span>
      </div>
      );
  };
}

export default Welcome;
