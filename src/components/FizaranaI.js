import React from 'react';
import Props from './Props'
import Counter from './Counter'



const FizaranaI = () => {
  return ( 
    <div>
      <Counter />
      <Props name="Tanjiro" swordColor="black" />
      <Props name="Inosuke" swordColor="grey">
        <p>This is a children props</p>
      </Props>
      <div>This stuff has really nothing to do here!</div>
      <Props name="Zenitsu" swordColor="yello" />
      <p>But in the end, all swords turn red!</p>
    </div>
  );
}


export default FizaranaI;
