import React, { useState } from 'react';

const Folder = (props) => {
  // state: array of two stuff: a var and a function
  const [ isOpen, setIsOpen ] = useState(false);
  const { name, children } = props ;

  const handleClick = () => setIsOpen(!isOpen);
  const the_eye = isOpen ? 'slash outline' : ''
  //const direction = isOpen ? 'down' : 'right'

  return <div>
    {/* semantic css library link put in index.html */}
    { /* https://semantic-ui.com/elements/icon.html */} 
    <span onClick={ handleClick }>
      <i className={`blue eye ${the_eye} icon`}></i>
      {/* <i className={`caret ${direction} icon`}></i> */}
    </span>
    <b onClick={ handleClick }>{ name }</b>
        <div style={{ marginLeft: '18px' }}>
          { isOpen ? children : null }
        </div>
    </div>
}


export default Folder;
