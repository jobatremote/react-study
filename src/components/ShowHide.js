import React, { useState } from 'react';
import Counter from './Counter'


const ShowHide = () => {

  const [ shown, setShown ] = useState(false)

  const hideMe = () => setShown(!shown)

  return <div>
    <span>
      <button onClick={ hideMe }>unhide</button>
    </span>
    <span>
      <strong>
        { shown ? <Counter /> : null }
      </strong>
    </span>
  </div>
}


export default ShowHide;
