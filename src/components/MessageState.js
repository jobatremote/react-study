import React, { Component } from 'react'

class Welcome extends Component {
  constructor() {
    super()
    this.state = {
      message: 'Welcome visitor!'
    }
  }
  changeMessage() {
    this.setState({
      message: 'Thx for clicking :)'
    })
  }
  
  render() {
    return (
      <div>
        <p>{this.state.message}</p>
        <button onClick={() => this.changeMessage()}>Change msg</button>
      </div>
      )
  }
}

export default Welcome;
