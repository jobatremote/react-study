import React from 'react'

const Props = (props) => {
  console.log(props)
  return (
    <div>
      <u>{props.name}</u> has his sword color <b>{props.swordColor}</b>
      <i>{props.children}</i>
    </div>
  );
}

export default Props;
