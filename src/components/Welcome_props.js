import React, { Component } from 'react'

class Welcome_props extends Component {
    render() {
        return <p>{this.props.name} with color {this.props.swordColor}</p>
    }
}

export default Welcome_props;
