import React, { Component } from 'react'

export default class SearchInput extends Component {
    constructor() {
        super();

        this.onValueChange = this.onValueChange.bind(this);
    }

    onValueChange(event) {
        // let input = React.findDOMNode(this.refs.input);
        
        this.props.update(event.target.value);
    }

    render() {
        return (
            <div className="search-input">
                <input
                    ref="input"
                    onChange={this.onValueChange}
                    value={this.props.value}
                    type="text"
                    placeholder="Type something..." />
            </div>
        )
    }
}
