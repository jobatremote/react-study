import React, { Component } from 'react'


export default class SearchItem extends Component {
    onItemClick(value, event) {
        this.props.filter(value);
    }

    render() {
        return (
            <li onClick={this.onItemClick.bind(this, this.props.value)} >{this.props.value}</li>
        )
    }
}

//React.render(<Search />, document.querySelector('.search'));
