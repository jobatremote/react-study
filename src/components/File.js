import React from 'react';

const File = (props) => {
  const { name } = props
  const extension = name.split('.')[1]
  const fileIcons = {
    '': 'info',
    out: 'steam symbol',
    txt: 'file alternate outline',
    mp3: 'red music',
    wav: 'audio description',
    xspf: 'play circle outline',
    mp4: 'video',
    mv: 'red studiovinari',
    mkv: 'video'
  }
  return <div>
      <i className={`${fileIcons[extension]} icon`}></i>
      { name }
    </div>
}

export default File;
