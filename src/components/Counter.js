// https://www.youtube.com/watch?v=uirRaVjRsf4&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=11
import React, { Component } from 'react'

class Counter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0
    }
  }
  
  inc() {
    // to conserve last value
    this.setState(prevState => ({
    // this.setState((prevState, props) => ({
      count: prevState.count + 1
      // count: prevState.count + props.addValue
    }))
    console.log(this.state.count)
  }

  incFive() {
    this.inc()
    this.inc()
    this.inc()
    this.inc()
    this.inc()
  }

  render() {
    return (
      <div>
        <span style={{ marginRight: '12px' }}>
          <button onClick={() => this.incFive()}>++</button>
        </span>
          Count - <b>{this.state.count}</b>
      </div>
    )
  }
}

export default Counter;
