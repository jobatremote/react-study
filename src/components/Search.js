import React, { Component } from "react"
import SearchItem from './SearchItem'
import SearchInput from './SearchInput'

let classes = [ 'Temple Knight', 'Eva\'s Templar', 'Plains Walker', 'Wind Rider', 'Silver Ranger', 'Moonlight Sentinel', 'Spellsinger', 'Mystic Muse', 'Sword Singer', 'Sword Muse', 'Elemental Summoner', 'Elemental Master', 'Elven Elder', 'Eva\'s Saint'
];

export default class Search extends Component {

    constructor() {
        super();

        this.state = {
            list: classes,
            filteredList: [],
            searchString: ''
        }

        this.filterList = this.filterList.bind(this);
    }

    componentWillMount() {
        this.setState({filteredList: this.state.list});
    }

    filterList(value) {
        let searchValue = value.toLowerCase();
        let filteredList = this.state.list;

        filteredList = filteredList.filter(item => {
            return item.toLowerCase().search(searchValue) !== -1;
        });

        this.setState({filteredList: filteredList});
        this.setState({searchString: value});
    }

    render() {
        let searchItems = this.state.filteredList.map((item, index) => {
            return <SearchItem filter={this.filterList} key={index} value={item} />
        });

        return (
            <div className="search-component">
                <SearchInput value={this.state.searchString} update={this.filterList} />
                <ul className="search-list">
                    {searchItems}
                </ul>
            </div>
        )
    }
}
