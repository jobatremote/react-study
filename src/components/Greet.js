import React from 'react';

const Greet = (props) => {
  const { name, msg } = props;
  return <div>
      <h2>Hello { name }!</h2>
      <p>{ msg }</p>
    </div>
  
}

export default Greet;
