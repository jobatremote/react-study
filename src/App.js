import React from 'react';
import FizaranaI from './components/FizaranaI'
import Folder from './components/Folder'
import File from './components/File'
import Greet from './components/Greet'
import MessageState from './components/MessageState2'
import Counter from './components/Counter'
import ShowHide from './components/ShowHide'
import Search from './components/Search'

function App() {
	return <div>
      <h1>Salut!!! euh</h1>
      <ShowHide />
      <Search />

      <Greet name="Tamaho" msg="Nice to have you in this place :)"/>
      <MessageState />
      <Counter />
      <p>
        <Folder name='Desktop'>
          <File name='racourcis' />
          <File name='nohuup.out' />
          <File name='truc.txt' />
          <File name='zvt.mp3' />
          <Folder name='Mozika'>
            <Greet msg="Inside of"/>
            <File name='Franz.mp3' />
            <File name='Ravel.wav' />
            <File name='dance.xspf' />
          </Folder>
        </Folder>
        <Folder name='Video'>
          <FizaranaI />
          <br />
          <File name='tuts.mp4' />
          <File name='manga.mv' />
          <File name='concert.mp4' />
          <File name='moovie.mkv' />
        </Folder>
      </p>
		</div> 
}

export default App;
